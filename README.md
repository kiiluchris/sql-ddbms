This project is an attempt in interpreting SQL SELECT queries to localize them 
as for heterogenous databases that make up a distributed database system.

### TASK REQUIREMENTS
- Use a minimum of 2 operating systems
- Use a minimum of 3 relational databases
- Parse and process SQL queries for your created schema to 


### SYSTEM REQUIREMENTS
- Docker 
- Docker Compose
- SBT Scala Build Tool

### FILES
- *.sbt -> SBT Config files
- project/ -> SBT Config Files
- src/main/resources/application.conf -> Application Database Configuration
- conf/migrations -> Initialization SQL Queries for the databases
- conf/docker -> Docker Image setup for the ubuntu (postgres), archlinux (mariadb) and fedora (sqlserver) database servers.
- src/main/scala/model -> Records to represent the entities and value objects
- src/main/scala/repo -> Interface to query the external databases
- src/main/scala/sql -> Query processing and interpretation

### FRAGMENT PLACEMENT AT DB SITES
Site 1
- PHF: Admission grades for Moi University
- DHF: The addresses and students in the continents america and europe
- VF: The name and salary for lecturers

Site 2
- PHF: Admission grades for Nairobi University
- DHF: The addresses and students in the continents asia
- VF: The bonus,project and experience for lecturers

Site 3
- PHF: Admission grades for Kenyatta University
- DHF: The addresses and students in the continents africa

