CREATE TABLE roles (
  id MEDIUMINT AUTO_INCREMENT,
  role VARCHAR(50),
  role_short VARCHAR(5),
  PRIMARY KEY(id)
);
INSERT INTO roles (role, role_short)
VALUES
  ('Role 1', 'r-1'),
  ('Role 2', 'r-2'),
  ('Role 3', 'r-3'),
  ('Role 4', 'r-4'),
  ('Role 5', 'r-5'),
  ('Role 6', 'r-2'),
  ('Role 7', 'r-3'),
  ('Role 8', 'r-2'),
  ('Role 9', 'r-9'),
  ('Role 10', 'r-1');