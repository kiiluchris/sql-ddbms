CREATE TABLE grades (
  id MEDIUMINT,
  marks MEDIUMINT,
  university VARCHAR(100),
  PRIMARY KEY(id)
);
CREATE TABLE students (
  adm_no MEDIUMINT,
  name VARCHAR(100),
  marks MEDIUMINT,
  country VARCHAR(100),
  PRIMARY KEY(adm_no)
);
CREATE TABLE addresses (
  adm_no MEDIUMINT,
  city VARCHAR(100),
  country VARCHAR(100),
  PRIMARY KEY(adm_no)
);
CREATE TABLE lecturers (
  eid MEDIUMINT,
  project VARCHAR(100),
  experience MEDIUMINT,
  PRIMARY KEY(eid)
);
INSERT INTO grades (id, marks, university)
VALUES
  (1, 67, 'Nairobi University'),
  (5, 78, 'Nairobi University'),
  (6, 78, 'Nairobi University'),
  (8, 69, 'Nairobi University'),
  (0, 76, 'Nairobi University');
INSERT INTO students(adm_no, name, marks, country)
VALUES
  (1, 'Fazal', 22, 'Iraq'),
  (5, 'Mumraiz', 66, 'China'),
  (14, 'Nokia', 88, 'Japan'),
  (18, 'Mohamed', 92, 'Iran'),
  (22, 'Michael', 69, 'Iraq');
INSERT INTO addresses (adm_no, city, country)
VALUES
  (1, 'City A', 'Iraq'),
  (5, 'City E', 'China'),
  (14, 'City N', 'Japan'),
  (18, 'City R', 'Iran'),
  (22, 'City V', 'Iraq');
INSERT INTO lecturers (eid, project, experience)
VALUES
  (1, 'P1', 5),
  (2, 'P2', 4),
  (3, 'P3', 7),
  (4, 'P4', 8),
  (5, 'P10', 4),
  (6, 'P5', 5),
  (7, 'P6', 3),
  (8, 'P7', 4),
  (9, 'P8', 4),
  (10, 'P9', 5),
  (11, 'P11', 4),
  (12, 'P12', 6),
  (13, 'P13', 4),
  (14, 'P14', 7),
  (15, 'P15', 4),
  (16, 'P16', 8),
  (17, 'P17', 6),
  (18, 'P18', 5),
  (19, 'P19', 3),
  (20, 'P20', 5),
  (21, 'P21', 8),
  (22, 'P22', 7),
  (23, 'P23', 4);
