CREATE TABLE grades (
  id int PRIMARY KEY,
  marks int,
  university VARCHAR(100),
);
CREATE TABLE students (
  adm_no int PRIMARY KEY,
  name VARCHAR(100),
  marks int,
  country VARCHAR(100)
);
CREATE TABLE addresses (
  adm_no int PRIMARY KEY,
  city VARCHAR(100),
  country VARCHAR(100),
);
INSERT INTO grades (id, marks, university)
VALUES
  (3, 75, 'Kenyatta University'),
  (7, 63, 'Kenyatta University'),
  (9, 71, 'Kenyatta University'),
  (2, 72, 'Kenyatta University');
INSERT INTO students(adm_no, name, marks, country)
VALUES
  (4, 'Shahzeb', 90, 'Congo'),
  (6, 'Fazul', 82, 'Rwanda'),
  (8, 'Halima', 76, 'Tanzania'),
  (9, 'Jamal', 64, 'South Africa'),
  (11, 'Musa', 87, 'Congo'),
  (12, 'Andrew', 51, 'Rwanda'),
  (15, 'Kaite', 69, 'Tanzania'),
  (19, 'North', 75, 'Congo'),
  (20, 'Faith', 99, 'Rwanda'),
  (21, 'Naitah', 89, 'Nigeria');
INSERT INTO addresses (adm_no, city, country)
VALUES
  (4, 'City D', 'Congo'),
  (6, 'City F', 'Rwanda'),
  (8, 'City H', 'Tanzania'),
  (9, 'City I', 'South Africa'),
  (11, 'City K', 'Congo'),
  (12, 'City L', 'Rwanda'),
  (15, 'City O', 'Tanzania'),
  (19, 'City S', 'Congo'),
  (20, 'City T', 'Rwanda'),
  (21, 'City U', 'Nigeria');