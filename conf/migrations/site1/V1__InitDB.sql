CREATE TABLE users(
  id SERIAL PRIMARY KEY,
  name VARCHAR(50),
  role_id INTEGER
);
INSERT INTO users (name, role_id)
VALUES
  ('Name 1', 1),
  ('Name 2', 2),
  ('Name 3', 3),
  ('Name 4', 4),
  ('Name 5', 5),
  ('Name 6', 2),
  ('Name 7', 3),
  ('Name 8', 2),
  ('Name 9', 9),
  ('Name 10', 1);