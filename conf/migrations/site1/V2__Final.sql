CREATE TABLE grades (
  id INTEGER PRIMARY KEY,
  marks INTEGER,
  university VARCHAR(100)
);
CREATE TABLE students (
  adm_no INTEGER PRIMARY KEY,
  name VARCHAR(100),
  marks INTEGER,
  country VARCHAR(100)
);
CREATE TABLE addresses (
  adm_no INTEGER PRIMARY KEY,
  city VARCHAR(100),
  country VARCHAR(100)
);
CREATE TABLE lecturers (
  eid INTEGER PRIMARY KEY,
  name VARCHAR(100),
  salary INTEGER,
  bonus INTEGER
);
INSERT INTO grades (id, marks, university)
VALUES
  (4, 65, 'Moi University'),
  (2, 78, 'Moi University'),
  (1, 76, 'Moi University'),
  (3, 78, 'Moi University');
INSERT INTO students(adm_no, name, marks, country)
VALUES
  (2, 'Abdul', 66, 'Italy'),
  (3, 'Sameed', 77, 'UK'),
  (7, 'Zendeya', 57, 'UK'),
  (10, 'Kakai', 98, 'UK'),
  (13, 'Grove', 97, 'USA'),
  (16, 'Brenda', 78, 'USA'),
  (17, 'Abdul', 76, 'Italy'),
  (23, 'Charleen', 95, 'Greece'),
  (24, 'Kato', 96, 'Greece');
INSERT INTO addresses (adm_no, city, country)
VALUES
  (2, 'City B', 'Italy'),
  (3, 'City C', 'UK'),
  (7, 'City G', 'UK'),
  (10, 'City J', 'UK'),
  (13, 'City M', 'USA'),
  (16, 'City P', 'USA'),
  (17, 'City Q', 'Italy'),
  (23, 'City W', 'Greece'),
  (24, 'City X', 'Greece');
INSERT INTO lecturers (eid, name, salary, bonus)
VALUES
  (1, 'ABC', 50000, 5000),
  (2, 'DEF', 40000, 7000),
  (3, 'GHI', 45000, 8000),
  (4, 'JKL', 65000, 4000),
  (5, 'MN', 78000, 4800),
  (6, 'OP', 67800, 3000),
  (7, 'IU', 57890, 6021),
  (8, 'QW', 70980, 2000),
  (9, 'ERT', 89000, 5000),
  (10, 'TYU', 65000, 2000),
  (11, 'OPL', 76000, 1500),
  (12, 'QAZ', 110000, 23000),
  (13, 'SDS', 200000, 8000),
  (14, 'YHJ', 130000, 30000),
  (15, 'OPL', 75000, 4000),
  (16, 'IOU', 56000, 4000),
  (17, 'MNA', 90000, 5000),
  (18, 'RTY', 150000, 4000),
  (19, 'KLA', 300000, 32000),
  (20, 'POP', 89000, 2000),
  (21, 'YUO', 98000, 3000),
  (22, 'QWS', 150000, 20000),
  (23, 'FGH', 123444, 7889);