FROM fedora:28

RUN curl -o /etc/yum.repos.d/mssql-server.repo https://packages.microsoft.com/config/rhel/7/mssql-server-2019.repo &&\
    curl -o /etc/yum.repos.d/mssql-tools.repo https://packages.microsoft.com/config/rhel/7/prod.repo

LABEL RUN /usr/bin/docker run -d -t -v \${DATADIR}:/var/opt/mssql/data:rw,z -v \${LOGDIR}:/var/opt/mssql/log:rw,z -e ACCEPT_EULA=\${ACCEPT_EULA} -e SA_PASSWORD=\${SA_PASSWORD} IMAGE

# Change Fedora version if SQL Server does not install
# Prefer 28 - 30, Max 31, Lower is better
RUN dnf -y install mssql-server sudo wget

# RUN wget -qO- https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/6.3.1/flyway-commandline-6.3.1-linux-x64.tar.gz | tar xvz && ln -s `pwd`/flyway-6.3.1/flyway /usr/local/bin 

RUN ACCEPT_EULA=Y dnf -y install mssql-tools unixODBC-devel &&\
    ln -s /opt/mssql-tools/bin/sqlcmd /usr/bin/

RUN dnf -y install nc

# VOLUME [ "/var/opt/mssql/data" , "/var/opt/mssql/log"]
EXPOSE 1433
COPY ./conf/docker/site3-start.sh start.sh

# Migrate database
COPY ./conf/migrations/site3/ ./migrations 

CMD ["/start.sh"]
