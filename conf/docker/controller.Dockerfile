FROM mozilla/sbt

WORKDIR /app/

COPY . /app/
RUN sbt compile

CMD [ "sbt", "run" ]