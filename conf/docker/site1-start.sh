#!/bin/bash


/usr/lib/postgresql/9.3/bin/postgres -D "/var/lib/postgresql/9.3/main" \
    -c "config_file=/etc/postgresql/9.3/main/postgresql.conf" &

SQL_SERVER_PID=$!
counter=1
errstatus=1
echo -e "Init SQL Query\n$INIT_SQL"
while [ $counter -le 10 ] && [ $errstatus = 1 ]
do
    echo "Waiting for Postgres to start..."
    sleep 5
    psql -f "migrations/V1__InitDB.sql" 2>/dev/null
    errstatus=$?
    ((counter++))
done
if [ $errstatus = 1 ]
then
    kill -s9 $SQL_SERVER_PID
    echo "Cannot connect to Postgres, installation aborted"
    exit $errstatus
else
    echo "Initialized successfully"
fi

tail -f /dev/null


