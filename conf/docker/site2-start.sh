#!/bin/bash
MIGRATIONS=`cat migrations/V1__InitDB.sql`
MIGRATIONS2=`cat migrations/V2__Final.sql`

read -r -d '' INIT_SQL << EOM
CREATE USER IF NOT EXISTS 'docker'@'localhost' IDENTIFIED BY 'docker';
GRANT ALL PRIVILEGES ON *.* TO 'docker'@'localhost' WITH GRANT OPTION;
CREATE USER IF NOT EXISTS 'docker'@'%' IDENTIFIED BY 'docker';
GRANT ALL PRIVILEGES ON *.* TO 'docker'@'%' WITH GRANT OPTION;
DROP DATABASE IF EXISTS docker;
CREATE DATABASE docker;
USE docker;
$MIGRATIONS
$MIGRATIONS2
EOM

/usr/bin/mysqld --user=root --datadir='/var/lib/mysql' &
SQL_SERVER_PID=$!

counter=1
errstatus=1

while [ $counter -le 10 ] && [ $errstatus = 1 ]
do
    echo "Waiting for MariaDB to start..."
    
    sleep 5
    mysql -u root  <<-EOF
$INIT_SQL
EOF
    errstatus=$?
    ((counter++))
done
if [ $errstatus = 1 ]
then
    kill -s9 $SQL_SERVER_PID
    echo "Cannot connect to SQL Server, installation aborted"
    exit $errstatus
else
    echo "Initialized successfully"
fi


mysql -u docker -p 
