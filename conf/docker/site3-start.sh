#!/bin/bash
MIGRATIONS=`cat migrations/V1__InitDB.sql`
MIGRATIONS2=`cat migrations/V2__Final.sql`
read -r -d '' INIT_SQL << EOM
CREATE DATABASE docker;
GO
CREATE LOGIN docker 
    WITH PASSWORD='$SA_PASSWORD', DEFAULT_DATABASE=docker;
GO
ALTER SERVER ROLE [sysadmin] ADD MEMBER docker;
GO
CREATE USER docker FOR LOGIN docker;
GO
USE docker;
GO
$MIGRATIONS
$MIGRATIONS2
GO
EOM


if [ "$ACCEPT_EULA" == 'YES' ] && [ "$SA_PASSWORD" != '' ]; then
    if ! [ -f /sql-server-installed ]; then
	MSSQL_PID='Developer' 
    ACCEPT_EULA='Y' 
    MSSQL_SA_PASSWORD="$SA_PASSWORD" 
    /opt/mssql/bin/mssql-conf -n setup 
	echo  "--accept-eula $ACCEPT_EULA --set-sa-password $SA_PASSWORD"
	touch /sql-server-installed
    fi
    /opt/mssql/bin/sqlservr&
    SQL_SERVER_PID=$!
    counter=1
    errstatus=1
    while [ $counter -le 20 ] && [ $errstatus = 1 ]
    do
        echo "Waiting for MS SQL Server to start..."
        sleep 5
	nc -z localhost 1433 
        errstatus=$?
        ((counter++))
    done
    if [ $errstatus = 1 ]
    then
        kill -9 $SQL_SERVER_PID
        echo "Cannot connect to SQL Server, installation aborted"
        exit $errstatus
    fi
    echo "Setting up user information"
    sleep 2
    # sqlcmd -S localhost -U SA -P 'its2late&' -Q "
    sqlcmd -S localhost -U SA -P "$MSSQL_SA_PASSWORD" \
            -Q "$INIT_SQL"
    tail -f /dev/null
else
    echo -e "ACCEPT_EULA was \"$ACCEPT_EULA\"\nSA_PASSWORD was \"$SA_PASSWORD\"\nIn order to run this container you must provide a SA_PASSWORD and set ACCEPT_EULA=YES as env vars passed to docker run."
fi
