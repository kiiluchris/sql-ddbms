FROM archlinux:20200306


# wget for flyway, base-devel for makepkg
RUN pacman -Sy --noconfirm wget mariadb
# Mariadb creates user mysql

# Install Flyway migrations tool
# RUN wget -qO- https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/6.3.1/flyway-commandline-6.3.1-linux-x64.tar.gz | tar xvz && ln -s `pwd`/flyway-6.3.1/flyway /usr/local/bin 

# RUN mysql_secure_installation --help
RUN mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql 
# Two all-privilege accounts were created.
# One is root@localhost, it has no password, but you need to
# be system 'root' user to connect. Use, for example, sudo mysql
# The second is mysql@localhost, it has no password either, but
# you need to be the system 'mysql' user to connect.
# After connecting you can set the password, if you would need to be
# able to connect as any of these users with a password and without sudo


WORKDIR /usr
EXPOSE 3306

COPY ./conf/docker/site2-start.sh start.sh

# Migrate database
COPY ./conf/migrations/site2/ ./migrations 


CMD ["./start.sh"]
