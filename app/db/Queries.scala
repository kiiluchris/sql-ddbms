package db

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Try

object Queries {
  def studentByCountry(country: String) =
    s"SELECT s.* FROM students AS s WHERE s.country = '$country'"
  def addressByCountry(country: String) =
    s"SELECT s.* FROM addresses AS s WHERE s.country = '$country'"
  def gradesByUniversity(university: String) =
    s"SELECT s.* FROM grades AS s WHERE s.university = '$university'"


  val lecturersHRQuery = "SELECT l.id, l.name, l.salary, l.bonus FROM lecturers l"
  val lecturersDepartmentHeadQuery = "SELECT l.id, l.name, l.project, l.experience FROM lecturers l"

  val lecturers = "SELECT l.* FROM lecturers l"

  val sampleQueries = List(
    lecturers,
    lecturersHRQuery,
    lecturersDepartmentHeadQuery,
    studentByCountry("USA"),
    studentByCountry("China"),
    studentByCountry("Italy"),
    studentByCountry("Iran"),
    studentByCountry("Australia"),
    addressByCountry("USA"),
    addressByCountry("China"),
    addressByCountry("Italy"),
    addressByCountry("Iran"),
    addressByCountry("Australia"),
    gradesByUniversity("Moi University"),
    gradesByUniversity("Nairobi University"),
    gradesByUniversity("Kenyatta University")
  )

  object V1 {
    def query = {
      print("Do you want to input a custom query [y/N]? ")
      val runCustomQuery =
        Try {
          Await.result(
            Future { scala.io.StdIn.readLine().toLowerCase().startsWith("y") },
            3.seconds
          )
        }.fold(_ => false, x => x)
      if (runCustomQuery) scala.io.StdIn.readLine().trim()
      else """SELECT u.*, r.* FROM roles r
            |OUTER JOIN users u
            |ON  r.rid = u.rid """.stripMargin.trim
    }
  }
}
