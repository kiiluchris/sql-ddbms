package db.model

case object NullModel extends BaseModel(-1) {
  def getField(name: String): Option[Any] = None
}
