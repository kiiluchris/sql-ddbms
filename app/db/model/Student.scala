package db.model

case class Student(id: Int, name: String, marks: Int, country: String)
    extends BaseModel(id) {
  def getField(name: String): Option[Any] =
    name match {
      case "adm_no"      => Some(id)
      case "marks"   => Some(marks)
      case "name"    => Some(name)
      case "country" => Some(country)
      case _         => None
    }
}
