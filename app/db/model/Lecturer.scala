package db.model

case class Lecturer(
    id: Int,
    name: String,
    salary: Int,
    bonus: Int,
    project: String,
    experience: Int
) extends BaseModel(id) {
  def getField(name: String): Option[Any] =
    name match {
      case "id"         => Some(id)
      case "name"       => Some(name)
      case "salary"     => Some(salary)
      case "bonus"      => Some(bonus)
      case "project"    => Some(project)
      case "experience" => Some(experience)
      case _            => None
    }
}
