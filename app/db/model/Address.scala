package db.model

case class Address(id: Int, city: String, country: String)
    extends BaseModel(id) {
  def getField(name: String): Option[Any] =
    name match {
      case "adm_no"      => Some(id)
      case "city"    => Some(city)
      case "country" => Some(country)
      case _         => None
    }
}
