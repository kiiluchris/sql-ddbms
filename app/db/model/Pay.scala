package db.model

case class Pay(id: Int, amount: Int) extends BaseModel(id) {
  def getField(name: String): Option[Any] =
    name match {
      case "id"     => Some(id)
      case "amount" => Some(amount)
      case _        => None
    }
}
