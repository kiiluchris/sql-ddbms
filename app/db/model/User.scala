package db.model

import db.sql.schema.FragmentTableSchema

case class User(id: Int, name: String, roleId: Int) extends BaseModel(id) {
  def getField(name: String): Option[Any] =
    FragmentTableSchema.userSchema.indexedFields
      .find { fi => fi._1.name == name }
      .map {
        case (_, i) =>
          this.productElement(i)
      }
}
