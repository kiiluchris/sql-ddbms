package db.model

case class Grade(id: Int, marks: Int, university: String)
    extends BaseModel(id) {
  def getField(name: String): Option[Any] =
    name match {
      case "id"         => Some(id)
      case "marks"      => Some(marks)
      case "university" => Some(university)
      case _            => None
    }
}
