package db.model

abstract class BaseModel(id: Int) {
  val _id = id
  def getField(name: String): Option[Any]
}
