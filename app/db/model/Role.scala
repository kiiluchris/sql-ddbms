package db.model

import db.sql.schema.FragmentTableSchema

case class Role(id: Int, role: String, roleShort: String)
    extends BaseModel(id) {
  def getField(name: String): Option[Any] =
    FragmentTableSchema.roleSchema.indexedFields.find(_._1.name == name).map {
      case (_, i) =>
        this.productElement(i)
    }
}
