package db

object Dump {

  // implicit val system = ActorSystem()
  // implicit val materializer = ActorMaterializer()
  // implicit val executionContext = system.dispatcher
  // implicit object ColumnValueFormat extends RootJsonFormat[TableColumnValue] {
  //   def write(obj: TableColumnValue): JsValue =
  //     obj match {
  //       case ColumnValue(value, fieldType) =>
  //         val resVal = fieldType match {
  //           case StringT  => JsString("")
  //           case IntegerT => JsNumber(value.asInstanceOf[Int])
  //           case FloatT   => JsNumber(value.asInstanceOf[Float])
  //           case DoubleT  => JsNumber(value.asInstanceOf[Double])
  //           case DateT =>
  //             JsNumber(
  //               value.asInstanceOf[LocalDateTime].toEpochSecond(ZoneOffset.UTC)
  //             )
  //         }
  //         JsObject(
  //           Map(
  //             "value" -> resVal,
  //             "type" -> JsString(fieldType.toString())
  //           )
  //         )
  //       case ColumnNull => JsNull
  //     }
  //   def read(json: JsValue): TableColumnValue =
  //     json match {
  //       case JsNull => ColumnNull
  //       case JsObject(fields) =>
  //         (fields.get("type"), fields.get("value")) match {
  //           case (Some(JsString(fieldType)), Some(JsString(value)))
  //               if fieldType == StringT.toString() =>
  //             ColumnValue(value, StringT)
  //           case (Some(JsString(fieldType)), Some(JsNumber(value)))
  //               if fieldType == IntegerT.toString() =>
  //             ColumnValue(value, IntegerT)
  //           case (Some(JsString(fieldType)), Some(JsNumber(value)))
  //               if fieldType == DoubleT.toString() =>
  //             ColumnValue(value, DoubleT)
  //           case (Some(JsString(fieldType)), Some(JsNumber(value)))
  //               if fieldType == FloatT.toString() =>
  //             ColumnValue(value, FloatT)
  //           case (Some(JsString(fieldType)), Some(JsNumber(value)))
  //               if fieldType == DateT.toString() =>
  //             ColumnValue(
  //               LocalDateTime.ofEpochSecond(value.toLong, 0, ZoneOffset.UTC),
  //               DateT
  //             )
  //           case _ => deserializationError("Error parsing ColumnValue")
  //         }
  //       case _ => deserializationError("ColumnValue or null expected")
  //     }
  // }
}
