package db

import sql._
import sql.schema._
import db.model.User
import db.model.Role
import db.model.Pay
import db.model.BaseModel
import FragmentTableSchema._
import SqlParser.SqlParserSyntax
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Try
import scala.util.Failure
import scala.util.Success
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._
import spray.json._
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.http.scaladsl.Http
import scala.io.StdIn
import java.time.LocalDateTime
import java.time.ZoneOffset

object Db {
  val globalCatalog = DatabaseCatalog(
    // userSchema,
    // roleSchema,
    studentSchema,
    gradeSchema,
    lecturerSchema,
    addressSchema
  )
  def runQuery(
      sqlQuery: String
  ): Either[String, List[Map[String, TableColumnValue]]] = {
    println("RAW SQL")
    println(sqlQuery)
    println("\n")
    val globalSchema = globalCatalog.copy()
    val parsedOperator = Try(sqlQuery.parseSql) match {
      case Failure(exception) => return Left(exception.getMessage())
      case Success(value) => value
    }
    println("PARSED  OPERATIONS")
    println(parsedOperator)
    println("\n")
    val validated =
      QueryValidator
        .validateSchema(
          parsedOperator,
          globalSchema
        )
    if (validated.isLeft) {
      return validated.map(x => List())
    }
    println("VALIDATED OPERATIONS\n\n")
    val sqliteProcessor = new QueryLocalizer { self =>
      def explicitlyNamesOuterInJoins = false
      def aliasFormatter(tableName: String, alias: String): String =
        tableName + " AS " + alias
    }
    val localizedQuery = sqliteProcessor
      .execQuery(globalSchema)(
        parsedOperator
      )
      .asInstanceOf[List[QueryAST.Projection]]

    println("LOCALIZED QUERIES")
    localizedQuery.foreach { q => println(q); println("") }
    println("\n")
    val recordMap = localizedQuery
      .foldLeft(Map.empty[QueryAST.TableName, List[_]]) { (acc, q) =>
        val tableName =
          q.parent.asInstanceOf[QueryAST.OverallJoin].parent

        val schema = globalSchema.schema.get(tableName.name).get
        acc + (tableName -> schema.fetchRecords())
      }
    println("LOCALIZED RECORDS")
    println(recordMap)
    print("\n\n")
    val processedRecords =
      QueryInterpreter.execQuery(
        globalSchema,
        recordMap
          .asInstanceOf[Map[QueryAST.TableName, List[BaseModel]]]
      )(parsedOperator)
    println("PROCESSED RECORDS")
    processedRecords.foreach(println(_))
    println("\n\n")
    Right(processedRecords)
  }
}
