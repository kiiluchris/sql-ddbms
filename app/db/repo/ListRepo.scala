package db.repo

import scala.concurrent.ExecutionContext

case class ListRepo[T](records: List[T]) extends BaseRepo {
  type RepoModel = T
  def list(implicit ec: ExecutionContext): List[T] = records
}
