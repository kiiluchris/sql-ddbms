package db.repo

import slick.jdbc.JdbcBackend.DatabaseDef
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import db.model._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext

case class LecturersFragment2Repo[P <: JdbcProfile](
    databaseConfig: DatabaseConfig[P]
) extends BaseRepo {
  type RepoModel = (Int, String, Int)
  val db = databaseConfig.db
  import databaseConfig.profile.api._
  class Lecturers(tag: Tag) extends Table[RepoModel](tag, "lecturers") {
    def id = column[Int]("eid", O.PrimaryKey)
    def project = column[String]("project")
    def experience = column[Int]("experience")

    def * =
      (id, project, experience)
  }
  val query = TableQuery[Lecturers]

  def list(implicit ec: ExecutionContext): List[RepoModel] =
    Await.result(db.run(query.result.map(_.toList)), 60.second)
}
