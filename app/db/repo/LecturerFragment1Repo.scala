package db.repo

import slick.jdbc.JdbcBackend.DatabaseDef
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import db.model._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext

case class LecturersFragment1Repo[P <: JdbcProfile](
    databaseConfig: DatabaseConfig[P]
) extends BaseRepo {
  type RepoModel = (Int, String, Int, Int)
  val db = databaseConfig.db
  import databaseConfig.profile.api._
  class Lecturers(tag: Tag) extends Table[RepoModel](tag, "lecturers") {
    def id = column[Int]("eid", O.PrimaryKey)
    def name = column[String]("name")
    def salary = column[Int]("salary")
    def bonus = column[Int]("bonus")

    def * =
      (id, name, salary, bonus)
  }
  val query = TableQuery[Lecturers]

  def list(implicit ec: ExecutionContext): List[RepoModel] =
    Await.result(db.run(query.result.map(_.toList)), 60.second)
}
