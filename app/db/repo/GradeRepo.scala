package db.repo

import slick.jdbc.JdbcBackend.DatabaseDef
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import db.model._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext

case class GradeRepo[P <: JdbcProfile](
    databaseConfig: DatabaseConfig[P]
) extends BaseRepo {
  type RepoModel = Grade
  val db = databaseConfig.db
  import databaseConfig.profile.api._
  class Grades(tag: Tag) extends Table[RepoModel](tag, "grades") {
    def id = column[Int]("id", O.PrimaryKey)
    def marks = column[Int]("marks")
    def university = column[String]("university")

    def * =
      (id, marks, university) <> (Grade.tupled, Grade.unapply _)
  }
  val query = TableQuery[Grades]

  def list(implicit ec: ExecutionContext): List[RepoModel] =
    Await.result(db.run(query.result.map(_.toList)), 60.second)
}
