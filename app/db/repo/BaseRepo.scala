package db.repo

import scala.concurrent.ExecutionContext

trait BaseRepo {
  type RepoModel
  def list(implicit ec: ExecutionContext): List[RepoModel]
}
