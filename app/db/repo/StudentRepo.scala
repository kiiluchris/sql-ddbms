package db.repo

import slick.jdbc.JdbcBackend.DatabaseDef
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import db.model._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext

case class StudentRepo[P <: JdbcProfile](
    databaseConfig: DatabaseConfig[P]
) extends BaseRepo {
  type RepoModel = Student
  val db = databaseConfig.db
  import databaseConfig.profile.api._
  class Students(tag: Tag) extends Table[RepoModel](tag, "students") {
    def id = column[Int]("adm_no", O.PrimaryKey)
    def name = column[String]("name")
    def marks = column[Int]("marks")
    def country = column[String]("country")

    def * =
      (id, name, marks, country) <> (Student.tupled, Student.unapply _)
  }
  val query = TableQuery[Students]

  def list(implicit ec: ExecutionContext): List[RepoModel] =
    Await.result(db.run(query.result.map(_.toList)), 60.second)
}
