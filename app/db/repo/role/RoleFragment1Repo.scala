package db.repo.role

import slick.jdbc.JdbcBackend.DatabaseDef
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import db.model._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import db.repo.BaseRepo

case class RoleFragment1Repo[P <: JdbcProfile](
    databaseConfig: DatabaseConfig[P]
) extends BaseRepo {
  type RepoModel = (Int, String, String)
  val db = databaseConfig.db
  import databaseConfig.profile.api._
  class Roles(tag: Tag) extends Table[RepoModel](tag, "roles") {
    def id = column[Int]("id", O.PrimaryKey)
    def name = column[String]("role")
    def roleId = column[String]("role_short")

    def * =
      (id, name, roleId)
  }
  val query = TableQuery[Roles]

  def list(implicit ec: ExecutionContext): List[RepoModel] =
    Await.result(db.run(query.result.map(_.toList)), 60.second)
}
