package db.repo

import slick.jdbc.JdbcBackend.DatabaseDef
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import db.model._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext

case class AddressRepo[P <: JdbcProfile](
    databaseConfig: DatabaseConfig[P]
) extends BaseRepo {
  type RepoModel = Address
  val db = databaseConfig.db
  import databaseConfig.profile.api._
  class Addresses(tag: Tag) extends Table[RepoModel](tag, "addresses") {
    def id = column[Int]("adm_no", O.PrimaryKey)
    def city = column[String]("city")
    def country = column[String]("country")

    def * =
      (id, city, country) <> (Address.tupled, Address.unapply _)
  }
  val query = TableQuery[Addresses]

  def list(implicit ec: ExecutionContext): List[RepoModel] =
    Await.result(db.run(query.result.map(_.toList)), 60.second)
}
