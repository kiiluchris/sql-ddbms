package db.repo.user

import slick.jdbc.JdbcBackend.DatabaseDef
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import db.model._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import db.repo.BaseRepo

case class UserFragment2Repo[P <: JdbcProfile](
    databaseConfig: DatabaseConfig[P]
) extends BaseRepo {
  type RepoModel = (Int, Int)
  val db = databaseConfig.db
  import databaseConfig.profile.api._
  class Users(tag: Tag) extends Table[RepoModel](tag, "users") {
    def id = column[Int]("id", O.PrimaryKey)
    def roleId = column[Int]("role_id")

    def * =
      (id, roleId)
  }
  val query = TableQuery[Users]

  def list(implicit ec: ExecutionContext): List[RepoModel] =
    Await.result(db.run(query.result.map(_.toList)), 60.second)
}
