package db.repo.user

import slick.jdbc.JdbcBackend.DatabaseDef
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import db.model._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import db.repo.BaseRepo

case class UserFragment1Repo[P <: JdbcProfile](
    databaseConfig: DatabaseConfig[P]
) extends BaseRepo {
  type RepoModel = (Int, String)
  val db = databaseConfig.db
  import databaseConfig.profile.api._
  class Users(tag: Tag) extends Table[RepoModel](tag, "users") {
    def id = column[Int]("id", O.PrimaryKey)
    def name = column[String]("name")

    def * =
      (id, name)
  }
  val query = TableQuery[Users]

  def list(implicit ec: ExecutionContext): List[RepoModel] =
    Await.result(db.run(query.result.map(_.toList)), 60.second)
}
