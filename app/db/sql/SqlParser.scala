package db
package sql
import QueryAST._
import db.sql.schema.FragmentTableSchema.StringT
import db.sql.schema.FragmentTableSchema.IntegerT

object SqlParser {
  import scala.util.parsing.combinator._

  def parseSql(input: String) = Grammar.parseAll(input)

  object Grammar extends JavaTokenParsers with PackratParsers {

    implicit class StringAdditionalSyntax(str: String) {
      def ignoreCase: Parser[String] = ("""(?i)\Q""" + str + """\E""").r
    }

    def stm: Parser[Operator] =
      selectClause ~ fromClause ~ whereClause ~ groupClause ^^ {
        case p ~ s ~ f ~ g => g(f(p(s)))
      }
    def selectClause: Parser[Operator => Operator] =
      "select".ignoreCase ~> ("*" ^^ { _ => (op: Operator) =>
        Projection(Vector(), Vector(), op)
      } | fieldList ^^ {
        case (fs, fs1) => Projection(fs, fs1, _: Operator)
      })
    def fromClause: Parser[Operator] =
      "from".ignoreCase ~> joinClause
    def whereClause: Parser[Operator => Operator] =
      opt("where".ignoreCase ~> predicate ^^ { p => Filter(p, _: Operator) }) ^^ {
        _.getOrElse((op: Operator) => op)
      }

    def aggregateClause: Parser[AggregateOp] =
      opt(
        ("sum".ignoreCase ^^ (_ => Sum)) | ("count".ignoreCase ^^ (_ => Count))
      ) ^^ {
        _.getOrElse(Ident)
      }
    def groupClause: Parser[Operator => Operator] =
      opt(
        "group".ignoreCase ~> "by".ignoreCase ~> aggregateClause ~ (opt("(") ~>
          tableRefFieldIdent <~ opt(")")) ^^ {
          case p1 ~ p2 => Group(p2, p1, _: Operator)
        }
      ) ^^ { _.getOrElse((op: Operator) => op) }

    def crossJoinClause: Parser[List[TableName]] =
      repsep(tableIdent ~ opt(opt("as".ignoreCase) ~> tableIdent), ",") ^^ {
        _.map { case a ~ b => TableName(a, b) }
      }

    def joinClause: Parser[Operator] =
      crossJoinClause.flatMap { cross =>
        ((joinIdent <~ "join".ignoreCase) ~ tableIdent ~ opt(
          opt("as".ignoreCase) ~> tableIdent
        ) ~ ("on".ignoreCase ~> joinPredicate)).* ^^ { p =>
          p.map {
            case joinType ~ table ~ tableAlias ~ join =>
              Join(joinType, TableName(table, tableAlias), join)
          }
        } map { joins => OverallJoin(cross.head, cross.tail, joins) }

      }

    def fieldIdent: Parser[String] = """[\w\#]+""".r
    def tableIdent: Parser[String] = """[\w_\-/]+""".r | "?"
    def leftJoin: Parser[JoinType] = "left".ignoreCase ^^ (_ => LeftJoin)
    def rightJoin: Parser[JoinType] = "right".ignoreCase ^^ (_ => RightJoin)
    def innerJoin: Parser[JoinType] = "inner".ignoreCase ^^ (_ => InnerJoin)
    def outerJoin: Parser[JoinType] = "outer".ignoreCase ^^ (_ => OuterJoin)
    def joinIdent: Parser[JoinType] =
      opt(leftJoin | rightJoin | outerJoin | innerJoin) ^^ (_.getOrElse(
        LeftJoin
      ))
    def fieldList: Parser[(Aliases, Columns)] =
      repsep(tableRefFieldIdentOrAll ~ opt("as".ignoreCase ~> fieldIdent), ",") ^^ {
        fs2s =>
          val (fs, fs1) = fs2s.map { case a ~ b => (b, a) }.unzip
          (Aliases(fs: _*), Columns(fs1: _*))
      }
    def fieldIdList: Parser[Schema] =
      repsep(fieldIdent, ",") ^^ (fs => Schema(fs: _*))

    def tableRefFieldIdentOrAll: Parser[Field] =
      tableRefFieldIdent | tableRefFieldAll
    def tableRefFieldIdent: Parser[Field] =
      tableIdent ~ "." ~ fieldIdent ^^ {
        case t ~ _ ~ f => Field(f, t)
      }
    def tableRefFieldAll: Parser[Field] =
      tableIdent ~ "." ~ "*" ^^ {
        case t ~ _ ~ f => Field(f, t)
      }

    def joinPredicate: Parser[JoinPredicate] =
      (tableRefFieldIdent ~ "=" ~ tableRefFieldIdent) ^^ {
        case ta ~ _ ~ tb =>
          JoinPredicate(ta, tb)
      }

    def eqPredicate: Parser[Predicate] =
      ref ~ "=" ~ ref ^^ { case a ~ _ ~ b => Eq(a, b) }
    def ltPredicate: Parser[Predicate] =
      ref ~ "<" ~ ref ^^ { case a ~ _ ~ b => Lt(a, b) }
    def gtPredicate: Parser[Predicate] =
      ref ~ ">" ~ ref ^^ { case a ~ _ ~ b => Gt(a, b) }
    def predicate: Parser[Predicate] =
      eqPredicate | ltPredicate | gtPredicate
    def ref: Parser[Ref] =
      tableRefFieldIdent |
        """'[^']*'""".r ^^ (s => Value(s.drop(1).dropRight(1), StringT)) |
        """[0-9]+""".r ^^ (s => Value(s.toInt, IntegerT))

    def parseAll(input: String): Operator = parseAll(stm, input) match {
      case Success(res, _) => res
      case res =>
        throw new Exception(res.toString)
    }
  }

  implicit class SqlParserSyntax(query: String) {
    def parseSql: Operator = {
      SqlParser.parseSql(query)
    }
  }
}
