package db
package sql

import shapeless.{HList, HNil, ::, HMap}

import model.{BaseModel, NullModel}
import schema._
import QueryAST._
import db.sql.schema.FragmentTableSchema._
import scala.annotation.meta.field

trait QueryInterpreter {

  def partialProduct[T]: (List[List[T]], List[T]) => List[List[T]] =
    (acc, records) => acc.flatMap(x => records.map(r => x ::: List(r)))
  def cartesianProduct[T](
      lists: List[T]*
  ): List[List[T]] =
    lists.headOption.fold(List.empty[List[T]]) { h =>
      val head = h.map(List(_))
      lists.tail.foldLeft(head)(partialProduct(_, _))
    }

  type ExecutionResult = Map[String, TableColumnValue]
  type ConvertFn = (
      List[List[BaseModel]],
      Map[String, Int]
  ) => List[ExecutionResult]
  type FilterFn =
    (
        List[BaseModel],
        Map[String, Int],
        ExecutionResult
    ) => Option[ExecutionResult]
  def emptyConvertFn: ConvertFn = (_, _) => List(Map())
  def emptyFilterFn: FilterFn = (_, _, x) => Some(x)

  def filterRecords(
      globalCatalog: DatabaseCatalog[String],
      recordMapping: Map[String, Int],
      sourceRecords: List[BaseModel],
      leftRef: Ref,
      rightRef: Ref,
      result: ExecutionResult
  )(predicate: (FieldType, Any, Any) => Boolean): Option[ExecutionResult] = {
    (leftRef, rightRef) match {
      case (field: Field, Value(value, valueType)) =>
        for {
          tableIndex <- recordMapping.get(field.tableName)
          fieldValue <- sourceRecords(tableIndex).getField(field.name)
          fieldType <- globalCatalog
            .fieldType(field.tableName, field.name)
          if fieldType == valueType
          if predicate(fieldType, fieldValue, value)
        } yield result
      case (Value(value, valueType), field: Field) =>
        for {
          tableIndex <- recordMapping.get(field.tableName)
          fieldValue <- sourceRecords(tableIndex).getField(field.name)
          fieldType <- globalCatalog
            .fieldType(field.tableName, field.name)
          if fieldType == valueType
          if predicate(fieldType, value, fieldValue)
        } yield result
      case (a: Field, b: Field) =>
        for {
          indexA <- recordMapping.get(a.tableName)
          indexB <- recordMapping.get(b.tableName)
          valueA <- sourceRecords(indexA).getField(a.name)
          valueB <- sourceRecords(indexB).getField(b.name)
          fieldTypeA <- globalCatalog.fieldType(a.tableName, a.name)
          fieldTypeB <- globalCatalog.fieldType(b.tableName, b.name)
          if fieldTypeA == fieldTypeB
          if predicate(fieldTypeA, valueA, valueB)
        } yield result
      case (Value(valueA, valueTypeA), Value(valueB, valueTypeB)) =>
        Option.when(valueTypeA == valueTypeB && valueA == valueB)(result)
    }

  }

  def execOp(
      globalCatalog: DatabaseCatalog[String],
      requestedRecords: Map[TableName, List[BaseModel]],
      op: Operator
  )(
      convert: ConvertFn = emptyConvertFn,
      filterResults: FilterFn = emptyFilterFn
  ): List[ExecutionResult] =
    op match {
      case Group(field, agg, parent) =>
        val tableName =
          globalCatalog.aliases.get(field.tableName).getOrElse(field.tableName)
        val tableSchema = globalCatalog.getSchema(tableName).get
        val fieldType = tableSchema.fieldType(field.name).get
        execOp(globalCatalog, requestedRecords, parent)()
          .foldLeft(Map.empty[String, ExecutionResult]) { (acc, record) =>
            record
              .get(field.formatAsSql) match {
              case None             => acc
              case Some(ColumnNull) => acc
              case Some(ColumnValue(recordValue, recordValueType)) =>
                agg match {
                  case Count =>
                    val accumulatorKey = recordValue.toString()
                    val countKey =
                      s"${field.formatAsSql}_count_${recordValue}"
                    acc.get(accumulatorKey) match {
                      case None =>
                        acc + (accumulatorKey -> (record + (countKey -> ColumnValue(
                          1,
                          IntegerT
                        ))))
                      case Some(currentAggregate) =>
                        val currentCount =
                          currentAggregate
                            .get(countKey)
                            .fold(0) {
                              case ColumnNull => 0
                              case ColumnValue(value, fieldType) =>
                                value.asInstanceOf[Int]
                            }
                        val nextCount = currentCount + 1
                        acc + (accumulatorKey -> (record + (countKey -> ColumnValue(
                          nextCount,
                          IntegerT
                        ))))
                    }
                  case Sum =>
                    val accumulatorKey = recordValue.toString()
                    val aggregateKey =
                      s"${field.formatAsSql}_sum_${recordValue}"
                    recordValueType match {
                      case StringT => acc
                      case DateT   => acc
                      case t @ _ =>
                        acc.get(accumulatorKey) match {
                          case None =>
                            acc + (accumulatorKey -> (record + (aggregateKey -> ColumnValue(
                              recordValue,
                              recordValueType
                            ))))
                          case Some(currentAggregate) =>
                            val currentSum =
                              currentAggregate
                                .get(aggregateKey)
                                .fold(t.default) {
                                  case ColumnValue(value, _) =>
                                    t.cast(value)
                                  case ColumnNull =>
                                    t.default
                                }
                            val updatedSum =
                              t.add(currentSum, t.cast(recordValue))
                            acc + (accumulatorKey -> (record + (aggregateKey -> ColumnValue(
                              updatedSum,
                              t
                            ))))
                        }
                    }
                  case Ident => acc + (recordValue.toString() -> record)
                }
            }
          }
          .values
          .toList
      case Filter(pred, parent: Projection) =>
        execOp(globalCatalog, requestedRecords, parent)(
          filterResults = { (sourceRecords, recordMapping, result) =>
            pred match {
              case Eq(a, b) =>
                filterRecords(
                  globalCatalog,
                  recordMapping,
                  sourceRecords,
                  a,
                  b,
                  result
                )((t, x, y) => t.eq(x, y))
              case Lt(a, b) =>
                filterRecords(
                  globalCatalog,
                  recordMapping,
                  sourceRecords,
                  a,
                  b,
                  result
                )((t, x, y) => t.lt(x, y))
              case Gt(a, b) =>
                filterRecords(
                  globalCatalog,
                  recordMapping,
                  sourceRecords,
                  a,
                  b,
                  result
                )((t, x, y) => t.gt(x, y))
            }
          }
        )
      case Projection(columnAliases, columns, p) =>
        val parent = p.asInstanceOf[OverallJoin]
        val columnsWithAliases = columnAliases.zip(columns)
        execOp(globalCatalog, requestedRecords, parent) {
          (results, tableIndexMap) =>
            {
              results.map { tableRecords =>
                val res = columnsWithAliases.foldRight(
                  Map().asInstanceOf[ExecutionResult]
                ) {
                  case ((aliasOpt, field), acc) =>
                    tableIndexMap.get(field.tableName) match {
                      case Some(index) =>
                        val tableName = field.tableName
                        val fields: List[Field] =
                          if (field.name == "*")
                            globalCatalog
                              .getSchema(tableName)
                              .get
                              .fields
                              .map(f => Field(f.name, tableName))
                          else List(field)
                        fields.foldLeft(acc) { (acc, f) =>
                          globalCatalog
                            .fieldType(tableName, f.name) match {
                            case None =>
                              acc + (
                                f.formatAsSql(aliasOpt) -> ColumnNull
                              )
                            case Some(fieldType) =>
                              acc + (
                                f.formatAsSql(aliasOpt) ->
                                  tableRecords(index)
                                    .getField(f.name)
                                    .map(ColumnValue(_, fieldType))
                                    .getOrElse(ColumnNull)
                              )
                          }
                        }
                      case None =>
                        throw new Exception(
                          s"Table Records: $tableIndexMap do not contain field $field"
                        )
                    }
                }
                filterResults(tableRecords, tableIndexMap, res)
              }.flatten
            }
        }
      case OverallJoin(parent, cross, joins) =>
        val parentRecords = requestedRecords.get(parent).get
        val crossRecords = cross.map(t => requestedRecords.get(t).get)
        val crossJoinedRecords =
          cartesianProduct(parentRecords :: crossRecords: _*)
        val crossTableIndices = (parent :: cross).zipWithIndex
          .flatMap {
            case (t, i) => List(Some(t.name -> i), t.alias.map((_, i)))
          }
          .collect {
            case Some(t) => t
          }
        val crossTableIndexMap: collection.mutable.Map[String, Int] =
          collection.mutable.Map(crossTableIndices: _*)
        val tableNames = (parent :: cross).map(_.name)
        var currentNextIndex = crossRecords.size + 1
        val allJoinedRecords = joins.foldLeft(crossJoinedRecords) {
          (acc, join) =>
            val joinType =
              join.joinType.asInstanceOf[JoinType]
            val leftPredicate = join.predicate.left
            val rightPredicate = join.predicate.right
            val leftTableName = leftPredicate.tableName
            val leftTableIndex = crossTableIndexMap.get(leftTableName)
            val rightTableName = rightPredicate.tableName
            val rightTableIndex = crossTableIndexMap.get(rightTableName)
            val records = requestedRecords.get(join.tableName).get
            val mutableRecords = collection.mutable.Set(records: _*)
            crossTableIndexMap(join.tableName.name) = currentNextIndex
            join.tableName.alias.foreach { alias =>
              crossTableIndexMap(alias) = currentNextIndex
            }
            currentNextIndex += 1
            (leftTableIndex, rightTableIndex) match {
              case (None, None) => crossJoinedRecords
              case (Some(left), Some(right)) =>
                crossJoinedRecords
                  .filter(cRecord =>
                    joinType match {
                      case InnerJoin => cRecord(left)._id == cRecord(right)._id
                      case OuterJoin => true
                      case LeftJoin  => cRecord(left) != NullModel
                      case RightJoin => cRecord(right) != NullModel
                    }
                  )

              case (None, Some(crossRight)) =>
                val joinedRecords = crossJoinedRecords.view
                  .map { cRecord =>
                    val right = cRecord(crossRight)
                    val rightField = right.getField(
                      rightPredicate.name
                    )
                    val leftRecord =
                      records
                        .find(r =>
                          r.getField(leftPredicate.name).isDefined && r
                            .getField(
                              leftPredicate.name
                            ) == rightField
                        )
                    leftRecord.foreach(l => mutableRecords.remove(l))
                    (joinType, leftRecord) match {
                      case (InnerJoin, Some(left))
                          if left.getField(leftPredicate.name) == rightField =>
                        Some(cRecord ::: List(left))
                      case (OuterJoin, left) =>
                        Some(cRecord ::: List(left.getOrElse(NullModel)))
                      case (LeftJoin, Some(left))
                          if left.getField(leftPredicate.name).isDefined =>
                        Some(cRecord ::: List(left))
                      case (RightJoin, left)
                          if right.getField(rightPredicate.name).isDefined =>
                        Some(cRecord ::: List(left.getOrElse(NullModel)))
                      case q @ _ =>
                        None
                    }
                  }
                  .flatten
                  .toList
                joinedRecords ++ (joinedRecords.headOption match {
                  case Some(value)
                      if List(OuterJoin, LeftJoin).contains(joinType) =>
                    mutableRecords.map(r =>
                      List.fill(value.size - 1)(NullModel) ++ List(r)
                    )
                  case _ => Nil
                })
              case (Some(crossLeft), None) =>
                val joinedRecords = crossJoinedRecords.view
                  .map { cRecord =>
                    val left = cRecord(crossLeft)
                    val leftField = left.getField(
                      leftPredicate.name
                    )
                    val rightRecord =
                      records
                        .find(r =>
                          r.getField(rightPredicate.name).isDefined && r
                            .getField(
                              rightPredicate.name
                            ) == leftField
                        )
                    rightRecord.foreach(l => mutableRecords.remove(l))
                    (joinType, rightRecord) match {
                      case (InnerJoin, Some(right))
                          if leftField == right
                            .getField(rightPredicate.name) =>
                        Some(cRecord ::: List(right))
                      case (OuterJoin, right) =>
                        Some(cRecord ::: List(right.getOrElse(NullModel)))
                      case (LeftJoin, right) if leftField.isDefined =>
                        Some(cRecord ::: List(right.getOrElse(NullModel)))
                      case (RightJoin, Some(right))
                          if right.getField(rightPredicate.name).isDefined =>
                        Some(cRecord ::: List(right))
                      case q @ _ =>
                        None
                    }
                  }
                  .flatten
                  .toList
                joinedRecords ++ (joinedRecords.headOption match {
                  case Some(value)
                      if List(OuterJoin, RightJoin).contains(joinType) =>
                    mutableRecords.map(r =>
                      List.fill(value.size - 1)(NullModel) ++ List(r)
                    )
                  case _ => Nil
                })
            }
        }
        convert(allJoinedRecords, crossTableIndexMap.toMap)
      case _ => List()
    }

  def isFieldValid(
      predicate: JoinPredicate
  ): (BaseModel, BaseModel) => Boolean =
    (left, right) => {

      val leftField = left.getField(predicate.left.name)
      val rightField = right
        .getField(predicate.right.name)
      (leftField, rightField) match {
        case (Some(l), Some(r)) if l == r => true
        case _                            => false
      }
    }

  private def noOp: ConvertFn = (_, _) => List()
  def execQuery(
      schema: DatabaseCatalog[String],
      requestedRecords: Map[TableName, List[BaseModel]]
  ): Operator => List[ExecutionResult] =
    execOp(schema, requestedRecords, _)(noOp)
}

object QueryInterpreter extends QueryInterpreter
