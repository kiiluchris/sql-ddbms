package db
package sql
import db.model.BaseModel
import schema._
import QueryAST._

trait QueryLocalizer extends QueryProcessor[List[_]] {
//   def explicitlyNamesOuterInJoins: Boolean

  def aliasFormatter(tableName: String, alias: String): String

  def execOp(globalCatalog: DatabaseCatalog[String], op: Operator)(
      convert: (String, Operator) => Operator
  ): List[Operator] =
    op match {
      case Projection(columnAliases, columns, parent) =>
        val groupedProjections =
          columns.zip(columnAliases).groupBy(_._1.tableName)
        execOp(globalCatalog, parent) { (tableName, op) =>
          val (columns, aliases) =
            groupedProjections
              .get(tableName)
              .fold({
                globalCatalog
                  .tableAlias(tableName)
                  .flatMap(tableAlias => groupedProjections.get(tableAlias))
                  .getOrElse(Vector.empty)
              })(x => x)
              .unzip
          Projection(aliases, columns, op)
        }
      case OverallJoin(parent, cross, joins) =>
        val tables = parent :: cross
        tables.map { table =>
          globalCatalog.setAlias(table.name, table.alias)
          convert(table.name, OverallJoin(table, List(), List()))
        } ++ joins.map { join =>
          globalCatalog.setAlias(join.tableName.name, join.tableName.alias)
          convert(
            join.tableName.name,
            OverallJoin(join.tableName, List(), List())
          )
        }
      case f @ Filter(pred, parent) =>
        execOp(globalCatalog, parent)((_, op) => op)
      case g @ Group(keys, agg, parent) =>
        execOp(globalCatalog, parent)((_, op) => op)

      case op => List()
    }
  def formatJoin(join: Join): String =
    List(
      join.joinTypeAsSql(this.explicitlyNamesOuterInJoins),
      join.tableName
        .formatAsSql(aliasFormatter _),
      "ON",
      join.predicate.formatAsSql
    ).mkString(" ")

  def execQuery(schema: DatabaseCatalog[String]): Operator => List[Operator] =
    execOp(schema, _)((_, op) => op)

}
