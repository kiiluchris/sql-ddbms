package db.sql.schema

import FragmentTableSchema.FieldType

case class TableColumn(name: String, columnType: FieldType)
