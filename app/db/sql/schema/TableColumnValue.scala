package db.sql.schema

import db.sql.schema.FragmentTableSchema.FieldType

sealed trait TableColumnValue {
  type Actual
  val value: Any
  def cast: Actual
}

final case class ColumnValue(value: Any, fieldType: FieldType)
    extends TableColumnValue {
  type Actual = fieldType.Actual
  override def cast = value.asInstanceOf[fieldType.Actual]
  override def toString: String = value.toString()
}

object ColumnNull extends TableColumnValue {
  type Actual = Unit
  val value = ()
  override def cast: Actual = ()
  override def toString: String = s"NULL"
}
