package db.sql.schema

import db.repo.BaseRepo
import db.repo.ListRepo
import db.sql.schema.FragmentTableSchema.FieldType
import scala.annotation.tailrec
import scala.concurrent.ExecutionContext

trait GlobalTableSchema[TableKey] { gSchema =>
  type Model
  type FragmentTableSchemaFields = List[TableColumn]
  type FSchema = FragmentTableSchema[TableKey, _ <: BaseRepo]
  type ResSchema[T] = FragmentTableSchema[TableKey, ListRepo[T]]
  type FRecord = x.repo.RepoModel forSome {
    val x: FragmentTableSchema[TableKey, _ <: BaseRepo]
  }
  type FRecords = List[FRecord]
  def tableIndex: TableColumn
  def fragments: List[FragmentTableSchema[TableKey, _ <: BaseRepo]]
  def tableName: TableKey
  def isDummyRun: Boolean = false
  final def fields: FragmentTableSchemaFields =
    fragments.flatMap(f => f.fields).distinctBy(col => col.name)
  final def indexedFields: List[(TableColumn, Int)] = fields.zipWithIndex

  def fetchRecords(dummyRun: Boolean = isDummyRun): List[Model]

  final def fieldType(field: String): Option[FieldType] = {
    fields.collectFirst {
      case TableColumn(name, fieldType) if name == field => fieldType
    }
  }

  final def schemaRecords(fragmentSchemata: FSchema*): FRecords =
    fragmentSchemata.flatMap(_.fetchRecords(isDummyRun)).toList

  final def schemaFields(fragmentSchemata: FSchema*): Seq[TableColumn] =
    fragmentSchemata.flatMap(_._fields).distinctBy(_.name)

  def horizontalJoin(fragmentSchemata: FSchema*): ResSchema[FRecord] = {
    val records = schemaRecords(fragmentSchemata: _*)
    val fields = schemaFields(fragmentSchemata: _*)
    FragmentTableSchema(gSchema, fields, records)
  }

  private def multiFlatMapRecords[F, G, T](
      fragmentSchemata: List[FSchema],
      fn: F => G
  ): List[T] =
    fragmentSchemata match {
      case Nil => List()
      case fx :: Nil =>
        fx.fetchRecords(isDummyRun).flatMap { record =>
          fn.asInstanceOf[fx.repo.RepoModel => List[T]](record)
        }
      case fx :: fy :: fs =>
        fx.fetchRecords(isDummyRun).flatMap { record =>
          multiFlatMapRecords(
            (fy :: fs),
            fn.asInstanceOf[fx.repo.RepoModel => fy.repo.RepoModel => _](record)
          )
        }
    }

  private def verticalJoin[T, F, G](
      fragmentSchemata: List[FSchema],
      fn: F => G
  ): ResSchema[T] =
    FragmentTableSchema(
      gSchema,
      schemaFields(fragmentSchemata: _*),
      multiFlatMapRecords(
        fragmentSchemata,
        fn
      )
    )

  def verticalJoin[T](f1: FSchema, f2: FSchema)(
      fn: f1.Model => f2.Model => List[T]
  ): ResSchema[T] = verticalJoin(List(f1, f2), fn)

  def verticalJoin[T](f1: FSchema, f2: FSchema, f3: FSchema)(
      fn: f1.Model => f2.Model => f3.Model => List[T]
  ): ResSchema[T] = verticalJoin(List(f1, f2, f3), fn)

  def verticalJoin[T](f1: FSchema, f2: FSchema, f3: FSchema, f4: FSchema)(
      fn: f1.Model => f2.Model => f3.Model => f4.Model => List[T]
  ): ResSchema[T] = verticalJoin(List(f1, f2, f3, f4), fn)

  def verticalJoin[T](
      f1: FSchema,
      f2: FSchema,
      f3: FSchema,
      f4: FSchema,
      f5: FSchema
  )(
      fn: f1.Model => f2.Model => f3.Model => f4.Model => f5.Model => List[T]
  ): ResSchema[T] = verticalJoin(List(f1, f2, f3, f4, f5), fn)

  def verticalJoin[T](
      f1: FSchema,
      f2: FSchema,
      f3: FSchema,
      f4: FSchema,
      f5: FSchema,
      f6: FSchema
  )(
      fn: f1.Model => f2.Model => f3.Model => f4.Model => f5.Model => f6.Model => List[
        T
      ]
  ): ResSchema[T] = verticalJoin(List(f1, f2, f3, f4, f5, f6), fn)

  def verticalJoin[T](
      f1: FSchema,
      f2: FSchema,
      f3: FSchema,
      f4: FSchema,
      f5: FSchema,
      f6: FSchema,
      f7: FSchema
  )(
      fn: f1.Model => f2.Model => f3.Model => f4.Model => f5.Model => f6.Model => f7.Model => List[
        T
      ]
  ): ResSchema[T] = verticalJoin(List(f1, f2, f3, f4, f5, f6, f7), fn)

  def verticalJoin[T](
      f1: FSchema,
      f2: FSchema,
      f3: FSchema,
      f4: FSchema,
      f5: FSchema,
      f6: FSchema,
      f7: FSchema,
      f8: FSchema
  )(
      fn: f1.Model => f2.Model => f3.Model => f4.Model => f5.Model => f6.Model => f7.Model => f8.Model => List[
        T
      ]
  ): ResSchema[T] = verticalJoin(List(f1, f2, f3, f4, f5, f6, f7, f8), fn)

  def verticalJoin[T](
      f1: FSchema,
      f2: FSchema,
      f3: FSchema,
      f4: FSchema,
      f5: FSchema,
      f6: FSchema,
      f7: FSchema,
      f8: FSchema,
      f9: FSchema
  )(
      fn: f1.Model => f2.Model => f3.Model => f4.Model => f5.Model => f6.Model => f7.Model => f8.Model => f9.Model => List[
        T
      ]
  ): ResSchema[T] = verticalJoin(List(f1, f2, f3, f4, f5, f6, f7, f8, f9), fn)

  def verticalJoin[T](
      f1: FSchema,
      f2: FSchema,
      f3: FSchema,
      f4: FSchema,
      f5: FSchema,
      f6: FSchema,
      f7: FSchema,
      f8: FSchema,
      f9: FSchema,
      f10: FSchema
  )(
      fn: f1.Model => f2.Model => f3.Model => f4.Model => f5.Model => f6.Model => f7.Model => f8.Model => f9.Model => f10.Model => List[
        T
      ]
  ): ResSchema[T] =
    verticalJoin(List(f1, f2, f3, f4, f5, f6, f7, f8, f9, f10), fn)

  def verticalJoin[T](
      f1: FSchema,
      f2: FSchema,
      f3: FSchema,
      f4: FSchema,
      f5: FSchema,
      f6: FSchema,
      f7: FSchema,
      f8: FSchema,
      f9: FSchema,
      f10: FSchema,
      f11: FSchema
  )(
      fn: f1.Model => f2.Model => f3.Model => f4.Model => f5.Model => f6.Model => f7.Model => f8.Model => f9.Model => f10.Model => f11.Model => List[
        T
      ]
  ): ResSchema[T] =
    verticalJoin(List(f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11), fn)

  def verticalJoin[T](
      f1: FSchema,
      f2: FSchema,
      f3: FSchema,
      f4: FSchema,
      f5: FSchema,
      f6: FSchema,
      f7: FSchema,
      f8: FSchema,
      f9: FSchema,
      f10: FSchema,
      f11: FSchema,
      f12: FSchema
  )(
      fn: f1.Model => f2.Model => f3.Model => f4.Model => f5.Model => f6.Model => f7.Model => f8.Model => f9.Model => f10.Model => f11.Model => f12.Model => List[
        T
      ]
  ): ResSchema[T] =
    verticalJoin(List(f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12), fn)

}
