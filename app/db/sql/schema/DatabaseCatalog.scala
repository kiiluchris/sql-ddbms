package db
package sql
package schema

import sql.QueryAST.TableName
import db.sql.schema.FragmentTableSchema.FieldType
import db.sql.QueryAST.Field

case class DatabaseCatalog[KeyType](
    schema: Map[KeyType, GlobalTableSchema[KeyType]]
) {
  val aliases: collection.mutable.Map[KeyType, KeyType] =
    collection.mutable.Map()
  private def tableNameAliasMap: Map[KeyType, KeyType] =
    aliases.map(_.swap).toMap
  def asTables: String =
    schema.values.foldLeft("") { (acc, table) =>
      List(acc, s"Table ${table.tableName}", "Fields")
        .concat(
          table.fields.map(field => s"    ${field.name} -> ${field.columnType}")
        )
        .mkString("\n")
    }

  def getSchema(
      alternateKeys: KeyType*
  ): Option[GlobalTableSchema[KeyType]] = {
    alternateKeys
      .flatMap(k => k :: (aliases.get(k).fold(List.empty[KeyType])(List(_))))
      .collectFirst {
        case key if schema.contains(key) => schema.get(key).get
      }
  }

  def fieldType(fieldTable: KeyType, fieldName: String): Option[FieldType] = {
    for {
      table <- tableName(fieldTable)
      tableSchema <- getSchema(table)
      fieldType <- tableSchema.fieldType(fieldName)
    } yield fieldType
  }

  def setAlias(tableName: KeyType, alias: Option[KeyType]): Boolean = {
    val nonExistingKey = alias.filter(!aliases.contains(_))
    nonExistingKey.foreach { a => aliases(a) = tableName }
    nonExistingKey.isDefined
  }

  def tableName(name: KeyType): Option[KeyType] = {
    if (schema.contains(name)) {
      Some(name)
    } else {
      aliases.get(name)
    }
  }

  def tableAlias(name: KeyType): Option[KeyType] = {
    tableNameAliasMap.get(name)
  }
}

object DatabaseCatalog {
  def apply[KeyType](
      schema: GlobalTableSchema[KeyType]*
  ): DatabaseCatalog[KeyType] =
    DatabaseCatalog(
      schema.foldLeft(Map.empty[KeyType, GlobalTableSchema[KeyType]]) {
        (acc, schema) => acc + (schema.tableName -> schema)
      }
    )
}
