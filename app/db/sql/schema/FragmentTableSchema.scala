package db
package sql
package schema

import java.time.LocalDateTime
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.H2Profile.api._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await
import scala.concurrent.duration._
import slick.lifted.ProvenShape
import slick.basic.DatabaseConfig
import slick.jdbc.PostgresProfile
import slick.jdbc.MySQLProfile
import slick.jdbc.SQLServerProfile
import slick.jdbc.JdbcProfile
import shapeless._

import db.model._
import db.sql.schema.FragmentTableSchema.FieldType
import db.repo.user._
import db.repo.role._
import db.repo._
import scala.collection.mutable

trait FragmentTableSchema[TableKey, Repo <: BaseRepo] {
  type FragmentTableSchemaFields = List[TableColumn]
  val repo: Repo
  type Model = repo.RepoModel
  def tableIndex: TableColumn
  def tableName: TableKey
  def _fields: FragmentTableSchemaFields
  final def fields: FragmentTableSchemaFields =
    tableIndex +: _fields
  final def indexedFields: List[(TableColumn, Int)] = fields.zipWithIndex

  final def fetchRecords(isDummyRun: Boolean): List[repo.RepoModel] =
    if (isDummyRun) fetchDummyRecords()
    else fetchDbRecords()

  def fetchDbRecords(): List[repo.RepoModel] = repo.list
  def fetchDummyRecords(): List[repo.RepoModel]

  final def fieldType(field: String): Option[FieldType] = {
    fields.collectFirst {
      case TableColumn(name, fieldType) if name == field => fieldType
    }
  }
}

object FragmentTableSchema {
  def apply[TableKey, T](
      globalSchema: GlobalTableSchema[TableKey],
      fields: Seq[TableColumn],
      records: List[T]
  ): FragmentTableSchema[TableKey, ListRepo[T]] =
    new FragmentTableSchema[TableKey, ListRepo[T]] {
      override val repo = ListRepo(records)
      override def tableName: TableKey = globalSchema.tableName
      override def tableIndex: TableColumn = globalSchema.tableIndex
      override def _fields: List[TableColumn] = fields.toList
      override def fetchDummyRecords(): List[T] = records
    }

  sealed trait FieldType {
    type Actual
    def default: Actual
    def cast(value: Any): Actual = value.asInstanceOf[Actual]
    def add(a: Actual, b: Actual): Actual

    private implicit class FieldAnyCastSyntax(value: Any) {
      def cast: Actual = value.asInstanceOf[Actual]
    }
    implicit def cmp: Ordering[_ >: Actual]
    def gt(a: Any, b: Any): Boolean =
      cmp.gt(a.cast, b.cast)
    def lt(a: Any, b: Any): Boolean =
      cmp.lt(a.cast, b.cast)
    def eq(a: Any, b: Any): Boolean =
      a.cast == b.cast

  }

  object StringT extends FieldType {
    type Actual = String
    override val cmp: Ordering[_ >: Actual] =
      implicitly[Ordering[Actual]]
    override def default: Actual = ""
    override def add(a: Actual, b: Actual): Actual = a + ", " + b

  }
  object IntegerT extends FieldType {
    type Actual = Int
    override val cmp =
      implicitly[Ordering[Actual]]
    override def default: Actual = 0
    override def add(a: Actual, b: Actual): Actual = a + b
  }
  object DateT extends FieldType {
    type Actual = LocalDateTime
    override val cmp: Ordering[_ >: Actual] =
      implicitly[Ordering[Actual]]
    override def add(a: Actual, b: Actual): Actual = b
    override def default: Actual = LocalDateTime.now()
  }
  object FloatT extends FieldType {
    type Actual = Float
    override val cmp: Ordering[_ >: Actual] =
      new Ordering[Float] {
        def compare(x: Float, y: Float): Int = (x - y).toInt
      }
    override def default: Actual = 0
    override def add(a: Actual, b: Actual): Actual = a + b

  }
  object DoubleT extends FieldType {
    type Actual = Double
    override val cmp: Ordering[_ >: Actual] =
      new Ordering[Actual] {
        def compare(x: Double, y: Double): Int = (x - y).toInt
      }
    override def default: Actual = 0.0
    override def add(a: Actual, b: Actual): Actual = a + b
  }

  // val postgresConfig = DatabaseConfig.forConfig("postgres")
  // val postgresDB = postgresConfig.db
  val postgresDBConfig =
    DatabaseConfig.forConfig[PostgresProfile]("postgres")
  val mySqlDBConfig = DatabaseConfig.forConfig[MySQLProfile]("mysql")
  val sqlServerDBConfig =
    DatabaseConfig.forConfig[SQLServerProfile]("sqlserver")
  val userF1Repo = UserFragment1Repo(postgresDBConfig)
  val userF2Repo = UserFragment2Repo(postgresDBConfig)
  val roleF1Repo = RoleFragment1Repo(mySqlDBConfig)
  val userFragment1 = new FragmentTableSchema[String, UserFragment1Repo[_]] {
    override val repo = userF1Repo
    override def tableIndex: TableColumn = TableColumn("uid", IntegerT)
    override def tableName = "users"
    override def _fields = List(
      TableColumn("name", StringT)
    )
    override def fetchDummyRecords: List[repo.RepoModel] = {
      (1 to 9).map { i => (i, s"Name $i") }.toList ++ List(
        (11, "Name 11")
      )
    }
  }
  val userFragment2 = new FragmentTableSchema[String, UserFragment2Repo[_]] {
    override val repo = userF2Repo
    override def tableIndex: TableColumn = TableColumn("uid", IntegerT)
    override def tableName = "users"
    override def _fields = List(
      TableColumn("rid", IntegerT)
    )
    override def fetchDummyRecords: List[repo.RepoModel] =
      (1 to 9).map { i => (i, 10 - i) }.toList ++ List(
        (11, 1)
      )
  }
  val userSchema = new GlobalTableSchema[String] {
    override type Model = User
    override def tableIndex: TableColumn = TableColumn("uid", IntegerT)
    override def fragments =
      List(userFragment1, userFragment2)
    override def tableName = "users"

    override def fetchRecords(dummyRun: Boolean) = {
      verticalJoin(userFragment1, userFragment2) { f1 => f2 =>
        if (f1._1 == f2._1) List(User(f1._1, f1._2, f2._2))
        else List()
      }.fetchRecords(dummyRun)
    }
  }

  val roleFragment1 = new FragmentTableSchema[String, RoleFragment1Repo[_]] {
    override val repo: RoleFragment1Repo[_] = roleF1Repo
    override def tableName: String = "roles"
    override def tableIndex: TableColumn = TableColumn("rid", IntegerT)

    override def _fields = List(
      TableColumn("role", StringT),
      TableColumn("role_short", StringT)
    )
    override def fetchDummyRecords =
      (1 to 10).map { i => (i, s"Name $i", s"r-$i") }.toList ++ List(
        (11, "Name 11", s"r-11")
      )
  }

  val roleSchema = new GlobalTableSchema[String] {
    override type Model = Role
    override def tableIndex: TableColumn = TableColumn("rid", IntegerT)
    override def tableName: String = "roles"
    override def fragments =
      List(roleFragment1)

    override def fetchRecords(dummyRun: Boolean): List[Role] = {
      roleFragment1
        .fetchRecords(dummyRun)
        .map(Role.tupled)
    }
  }
  

  val studentFragment1 = new FragmentTableSchema[String, StudentRepo[_]] {
    override val repo = StudentRepo(postgresDBConfig)
    override def tableName: String = "students"
    override def tableIndex: TableColumn = TableColumn("adm_no", IntegerT)

    override def _fields = List(
      TableColumn("name", StringT),
      TableColumn("marks", IntegerT),
      TableColumn("country", StringT)
    )
    override def fetchDummyRecords =
      List(
        Student(2, "Abdul", 66, "Italy"),
        Student(3, "Sameed", 77, "UK"),
        Student(7, "Zendeya", 57, "UK"),
        Student(10, "Kakai", 98, "UK"),
        Student(13, "Grove", 97, "USA"),
        Student(16, "Brenda", 78, "USA"),
        Student(17, "Abdul", 76, "Italy"),
        Student(23, "Charleen", 95, "Greece"),
        Student(24, "Kato", 96, "Greece")
      )
  }
  val studentFragment2 = new FragmentTableSchema[String, StudentRepo[_]] {
    override val repo = StudentRepo(mySqlDBConfig)
    override def tableName: String = "students"
    override def tableIndex: TableColumn = TableColumn("adm_no", IntegerT)

    override def _fields = List(
      TableColumn("name", StringT),
      TableColumn("marks", IntegerT),
      TableColumn("country", StringT)
    )
    override def fetchDummyRecords =
      List(
        Student(1, "Fazal", 22, "Iraq"),
        Student(5, "Mumraiz", 66, "China"),
        Student(14, "Nokia", 88, "Japan"),
        Student(18, "Mohamed", 92, "Iran"),
        Student(22, "Michael", 69, "Iraq")
      )
  }
  val studentFragment3 = new FragmentTableSchema[String, StudentRepo[_]] {
    override val repo = StudentRepo(sqlServerDBConfig)
    override def tableName: String = "students"
    override def tableIndex: TableColumn = TableColumn("adm_no", IntegerT)

    override def _fields = List(
      TableColumn("name", StringT),
      TableColumn("marks", IntegerT),
      TableColumn("country", StringT)
    )
    override def fetchDummyRecords =
      List(
        Student(4, "Shahzeb", 90, "Congo"),
        Student(6, "Fazul", 82, "Rwanda"),
        Student(8, "Halima", 76, "Tanzania"),
        Student(9, "Jamal", 64, "South Africa"),
        Student(11, "Musa", 87, "Congo"),
        Student(12, "Andrew", 51, "Rwanda"),
        Student(15, "Kaite", 69, "Tanzania"),
        Student(19, "North", 75, "Congo"),
        Student(20, "Faith", 99, "Rwanda"),
        Student(21, "Naitah", 89, "Nigeria")
      )
  }

  val studentSchema = new GlobalTableSchema[String] {
    override type Model = Student
    override def tableIndex: TableColumn = TableColumn("id", IntegerT)
    override def tableName: String = "students"
    override def fragments =
      List(studentFragment1, studentFragment2, studentFragment3)

    override def fetchRecords(dummyRun: Boolean): List[Student] = {
      horizontalJoin(studentFragment1, studentFragment2, studentFragment3)
        .fetchRecords(dummyRun)
        .asInstanceOf[List[Model]]
    }
  }
  val gradesFragment1 = new FragmentTableSchema[String, GradeRepo[_]] {
    override val repo = GradeRepo(postgresDBConfig)
    override def tableName: String = "grades"
    override def tableIndex: TableColumn = TableColumn("id", IntegerT)

    override def _fields = List(
      TableColumn("marks", IntegerT),
      TableColumn("university", StringT)
    )
    override def fetchDummyRecords =
      List(
        Grade(4, 65, "Moi University"),
        Grade(2, 78, "Moi University"),
        Grade(1, 76, "Moi University"),
        Grade(3, 78, "Moi University")
      )
  }
  val gradesFragment2 = new FragmentTableSchema[String, GradeRepo[_]] {
    override val repo = GradeRepo(mySqlDBConfig)
    override def tableName: String = "grades"
    override def tableIndex: TableColumn = TableColumn("id", IntegerT)

    override def _fields = List(
      TableColumn("marks", IntegerT),
      TableColumn("university", StringT)
    )
    override def fetchDummyRecords =
      List(
        Grade(1, 67, "Nairobi University"),
        Grade(5, 78, "Nairobi University"),
        Grade(6, 78, "Nairobi University"),
        Grade(8, 69, "Nairobi University"),
        Grade(0, 76, "Nairobi University")
      )
  }

  val gradesFragment3 = new FragmentTableSchema[String, GradeRepo[_]] {
    override val repo = GradeRepo(sqlServerDBConfig)
    override def tableName: String = "grades"
    override def tableIndex: TableColumn = TableColumn("id", IntegerT)

    override def _fields = List(
      TableColumn("marks", IntegerT),
      TableColumn("university", StringT)
    )
    override def fetchDummyRecords =
      List(
        Grade(3, 75, "Kenyatta University"),
        Grade(7, 63, "Kenyatta University"),
        Grade(9, 71, "Kenyatta University"),
        Grade(2, 72, "Kenyatta University")
      )
  }
  val gradeSchema = new GlobalTableSchema[String] {
    override type Model = Grade
    override def tableIndex: TableColumn = TableColumn("id", IntegerT)
    override def tableName: String = "grades"
    override def fragments =
      List(gradesFragment1, gradesFragment2, gradesFragment3)

    override def fetchRecords(dummyRun: Boolean): List[Grade] = {
      horizontalJoin(gradesFragment1, gradesFragment2, gradesFragment3)
        .fetchRecords(dummyRun)
        .asInstanceOf[List[Model]]
    }
  }
  val addressesFragment1 = new FragmentTableSchema[String, AddressRepo[_]] {
    override val repo = AddressRepo(postgresDBConfig)
    override def tableName: String = "addresses"
    override def tableIndex: TableColumn = TableColumn("adm_no", IntegerT)

    override def _fields = List(
      TableColumn("city", StringT),
      TableColumn("country", StringT)
    )
    override def fetchDummyRecords =
      List(
        Address(2, "City B", "Italy"),
        Address(3, "City C", "UK"),
        Address(7, "City G", "UK"),
        Address(10, "City J", "UK"),
        Address(13, "City M", "USA"),
        Address(16, "City P", "USA"),
        Address(17, "City Q", "Italy"),
        Address(23, "City W", "Greece"),
        Address(24, "City X", "Greece")
      )
  }
  val addressesFragment2 = new FragmentTableSchema[String, AddressRepo[_]] {
    override val repo = AddressRepo(mySqlDBConfig)
    override def tableName: String = "addresses"
    override def tableIndex: TableColumn = TableColumn("adm_no", IntegerT)

    override def _fields = List(
      TableColumn("city", StringT),
      TableColumn("country", StringT)
    )
    override def fetchDummyRecords =
      List(
        Address(1, "City A", "Iraq"),
        Address(5, "City E", "China"),
        Address(14, "City N", "Japan"),
        Address(18, "City R", "Iran"),
        Address(22, "City V", "Iraq")
      )
  }
  val addressesFragment3 = new FragmentTableSchema[String, AddressRepo[_]] {
    override val repo = AddressRepo(sqlServerDBConfig)
    override def tableName: String = "addresses"
    override def tableIndex: TableColumn = TableColumn("adm_no", IntegerT)

    override def _fields = List(
      TableColumn("city", StringT),
      TableColumn("country", StringT)
    )
    override def fetchDummyRecords =
      List(
        Address(4, "City D", "Congo"),
        Address(6, "City F", "Rwanda"),
        Address(8, "City H", "Tanzania"),
        Address(9, "City I", "South Africa"),
        Address(11, "City K", "Congo"),
        Address(12, "City L", "Rwanda"),
        Address(15, "City O", "Tanzania"),
        Address(19, "City S", "Congo"),
        Address(20, "City T", "Rwanda"),
        Address(21, "City U", "Nigeria")
      )
  }
  val addressSchema = new GlobalTableSchema[String] {
    override type Model = Address
    override def tableIndex: TableColumn = TableColumn("id", IntegerT)
    override def tableName: String = "addresses"
    override def fragments =
      List(addressesFragment1, addressesFragment2, addressesFragment3)

    override def fetchRecords(dummyRun: Boolean): List[Address] = {
      horizontalJoin(addressesFragment1, addressesFragment2, addressesFragment3)
        .fetchRecords(dummyRun)
        .asInstanceOf[List[Model]]
    }
  }
  val lecturerFragment1 =
    new FragmentTableSchema[String, LecturersFragment1Repo[_]] {
      override val repo = LecturersFragment1Repo(postgresDBConfig)
      override def tableName: String = "lecturers"
      override def tableIndex: TableColumn = TableColumn("id", IntegerT)

      override def _fields = List(
        TableColumn("name", StringT),
        TableColumn("salary", IntegerT),
        TableColumn("bonus", IntegerT)
      )
      override def fetchDummyRecords =
        List(
          (1, "ABC", 50000,5000),
          (2, "DEF", 40000,7000),
          (3, "GHI", 45000,8000),
          (4, "JKL", 65000,4000),
          (5, "MN", 78000,4800),
          (6, "OP", 67800,3000),
          (7, "IU", 57890,6021),
          (8, "QW", 70980,2000),
          (9, "ERT", 89000,5000),
          (10, "TYU", 65000,2000),
          (11, "OPL", 76000,1500),
          (12, "QAZ", 110000,23000),
          (13, "SDS", 200000,8000),
          (14, "YHJ", 130000,30000),
          (15, "OPL", 75000,4000),
          (16, "IOU", 56000,4000),
          (17, "MNA", 90000,5000),
          (18, "RTY", 150000,4000),
          (19, "KLA", 300000,32000),
          (20, "POP", 89000,2000),
          (21, "YUO", 98000,3000),
          (22, "QWS", 150000,20000),
          (23, "FGH", 123444,7889)
        )
    }
  val lecturerFragment2 =
    new FragmentTableSchema[String, LecturersFragment2Repo[_]] {
      override val repo = LecturersFragment2Repo(mySqlDBConfig)
      override def tableName: String = "lecturers"
      override def tableIndex: TableColumn = TableColumn("id", IntegerT)

      override def _fields = List(
        TableColumn("project", StringT),
        TableColumn("experience", IntegerT)
      )
      override def fetchDummyRecords =
        List(
          (1,  "P1", 5),
          (2,  "P2", 4),
          (3,  "P3", 7),
          (4,  "P4", 8),
          (5,  "P10", 4),
          (6,  "P5", 5),
          (7,  "P6", 3),
          (8,  "P7", 4),
          (9,  "P8", 4),
          (10,  "P9", 5),
          (11,  "P11", 4),
          (12,  "P12", 6),
          (13,  "P13", 4),
          (14,  "P14", 7),
          (15,  "P15", 4),
          (16,  "P16", 8),
          (17,  "P17", 6),
          (18,  "P18", 5),
          (19,  "P19", 3),
          (20,  "P20", 5),
          (21,  "P21", 8),
          (22,  "P22", 7),
          (23,  "P23", 4)
        )
    }

  val lecturerSchema = new GlobalTableSchema[String] {
    override type Model = Lecturer
    override def tableIndex: TableColumn = TableColumn("id", IntegerT)
    override def tableName: String = "lecturers"
    override def fragments =
      List(lecturerFragment1, lecturerFragment2)

    override def fetchRecords(dummyRun: Boolean): List[Lecturer] = {
      verticalJoin(lecturerFragment1, lecturerFragment2) { f1 => f2 =>
        if (f1._1 == f2._1)
          List(Lecturer(f1._1, f1._2, f1._3, f1._4, f2._2, f2._3))
        else List()
      }.fetchRecords(dummyRun)
        .asInstanceOf[List[Model]]
    }
  }
}
