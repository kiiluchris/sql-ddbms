package db
package sql

import db.sql.schema.FragmentTableSchema.FieldType

object QueryAST {
  type Table
  type Schema = Vector[String]
  type Aliases = Vector[Option[String]]
  type Columns = Vector[Field]

  sealed trait Operator
  object NoOp extends Operator
  case class Scan(
      name: Table,
      schema: Schema,
      delim: Char,
      extSchema: Boolean
  ) extends Operator
  case class PrintCSV(parent: Operator) extends Operator
  case class Projection(
      outSchema: Aliases,
      inSchema: Columns,
      parent: Operator
  ) extends Operator
  case class Filter(pred: Predicate, parent: Operator) extends Operator
  case class JoinOld(parent1: Operator, parent2: Operator) extends Operator
  case class Group(key: Field, agg: AggregateOp, parent: Operator)
      extends Operator
  case class HashJoin(parent1: Operator, parent2: Operator) extends Operator
  object NoJoin extends Operator
  case class OverallJoin(
      parent: TableName,
      cross: List[TableName],
      joins: List[Join]
  ) extends Operator
  case class Join(
      joinType: JoinType,
      tableName: TableName,
      predicate: JoinPredicate
  ) extends Operator {
    def joinTypeAsSql(isVerboseSyntax: Boolean): String =
      joinType match {
        case InnerJoin => "inner join"
        case LeftJoin  => if (isVerboseSyntax) "left outer join" else "join"
        case RightJoin =>
          if (isVerboseSyntax) "right outer join" else "right join"
        case OuterJoin => "full outer join"
      }
  }

  sealed trait JoinType {
    def asStr: String
  }
  object LeftJoin extends JoinType {
    def asStr: String = "join"
  }
  object RightJoin extends JoinType {
    def asStr: String = "right join"
  }
  object InnerJoin extends JoinType {
    def asStr: String = "inner join"
  }
  object OuterJoin extends JoinType {
    def asStr: String = "outer join"
  }

  case class JoinPredicate(left: Field, right: Field) {
    def formatAsSql: String =
      left.formatAsSql + " = " + right.formatAsSql
  }
  sealed trait Predicate
  case class Eq(a: Ref, b: Ref) extends Predicate
  case class Lt(a: Ref, b: Ref) extends Predicate
  case class Gt(a: Ref, b: Ref) extends Predicate

  sealed trait AggregateOp
  object Sum extends AggregateOp
  object Count extends AggregateOp
  object Ident extends AggregateOp

  sealed trait Ref
  case class Field(name: String, tableName: String) extends Ref {
    def formatAsSql: String = s"$tableName.$name"
    def formatAsSql(alias: Option[String]) =
      s"$tableName.${alias.getOrElse(name)}"
  }
  case class Value(x: Any, valueType: FieldType) extends Ref

  case class TableName(name: String, alias: Option[String]) {
    def formatAsSql(
        aliasFormatter: (String, String) => String
    ): String =
      alias
        .map { alias => aliasFormatter(name, alias) }
        .getOrElse(name)
  }
  object TableName {
    def apply(column: Field, alias: Option[String]): TableName = {
      TableName(column.formatAsSql, alias)
    }
  }

  def Schema(schema: String*): Schema = schema.toVector
  def Columns(columns: Field*): Columns = columns.toVector
  def Aliases(aliases: Option[String]*): Aliases = aliases.toVector
  def Scan(tableName: String): Scan = Scan(tableName, None, None)
  def Scan(
      tableName: String,
      schema: Option[Schema],
      delim: Option[Char]
  ): Scan = Scan(tableName, schema, delim)
}
