package db
package sql

import cats.syntax.either._

import schema._
import QueryAST._
import db.sql.schema.FragmentTableSchema.StringT
import db.sql.schema.FragmentTableSchema.IntegerT
import db.sql.schema.FragmentTableSchema.FieldType
import db.sql.schema.FragmentTableSchema.DateT
import db.sql.schema.FragmentTableSchema.FloatT
import db.sql.schema.FragmentTableSchema.DoubleT

trait QueryValidator {
  private def findColumnMatchingSchema[KeyType]
      : Columns => GlobalTableSchema[KeyType] => Boolean =
    columns =>
      table =>
        table.fields
          .find(field => columns.exists(_.name == field.name))
          .fold(false)(_ => true)
  private def findColumnNotMatchingSchema[KeyType]
      : Columns => GlobalTableSchema[KeyType] => Boolean =
    c => findColumnMatchingSchema(c) andThen (x => !x)
  def validateSchema[KeyType](
      operator: Operator,
      globalCatalog: DatabaseCatalog[String],
      aliasMap: Map[String, String] = Map()
  ): Either[String, Unit] = {
    operator match {
      case Projection(outSchema, inSchema, parent) =>
        outSchema
          .zip(inSchema)
          .find {
            case (alias, column) =>
              globalCatalog.schema
                .get(column.tableName)
                .map(
                  findColumnNotMatchingSchema(
                    inSchema.filter(_.tableName.contains(column.tableName))
                  )
                )
                .isDefined
          }
          .fold(validateSchema(parent, globalCatalog)) {
            case (alias, column) =>
              Left(
                s"""Projection "${column.formatAsSql}" does not exist in the global schema"""
              )
          }

      case OverallJoin(parent, cross, joins) =>
        val tables = parent :: cross ::: joins.map(_.tableName)
        tables.foreach(t => globalCatalog.setAlias(t.name, t.alias))
        val aliasMap_ = tables.collect {
          case t if t.alias.isDefined => (t.alias.get -> t.name)
        }.toMap
        val undefinedTable =
          tables.find(table =>
            globalCatalog.schema.get(table.name).isEmpty && aliasMap_
              .get(
                table.name
              )
              .isEmpty
          )
        undefinedTable.fold(
          joins
            .collectFirst(
              joinValidatePartial(globalCatalog)(aliasMap_) andThen {
                case validated if validated.isLeft => validated
              }
            )
            .getOrElse(eitherUnit)
        ) { table =>
          Left(
            s"""Table "${table.name}" does not exist in the global schema"""
          )
        }
      case Join(joinType, tableName, predicate) =>
        globalCatalog.schema
          .get(tableName.name)
          .fold(
            Left(s"Table ${tableName} does not exist in the global schema")
              .asInstanceOf[Either[String, Unit]]
          ) { _ =>
            globalCatalog.setAlias(tableName.name, tableName.alias)
            List(
              predicate.left,
              predicate.right
            ).find(t =>
                globalCatalog.schema
                  .get(t.tableName)
                  .isEmpty && aliasMap.get(t.tableName).isEmpty
              )
              .fold({
                (
                  isFieldInSchema(globalCatalog, aliasMap, predicate.left),
                  isFieldInSchema(globalCatalog, aliasMap, predicate.right)
                ) match {
                  case (true, true) => eitherUnit
                  case (false, false) =>
                    (s"Field ${predicate.left.name} is not in table ${aliasMap
                      .getOrElse(predicate.left.tableName, predicate.left.tableName)}" +
                      s"Field ${predicate.right.name} is also not in table ${aliasMap
                        .getOrElse(predicate.right.tableName, predicate.right.tableName)}")
                      .asLeft[Unit]
                  case (true, false) =>
                    s"Field ${predicate.right.name} is not in table ${aliasMap
                      .getOrElse(predicate.right.tableName, predicate.right.tableName)}"
                      .asLeft[Unit]
                  case (false, true) =>
                    s"Field ${predicate.left.name} is not in table ${aliasMap
                      .getOrElse(predicate.left.tableName, predicate.left.tableName)}"
                      .asLeft[Unit]
                }
              }) { actualPredicate =>
                Left(
                  s"Table ${actualPredicate.tableName} in join predicate ${actualPredicate.formatAsSql} does not exist in the global schema"
                )
              }
          }
      case Filter(pred, parent) =>
        validateSchema(parent, globalCatalog, aliasMap).flatMap { _ =>
          pred match {
            case Eq(a, b) =>
              validateFilter(a, b, "not equal to", globalCatalog) {
                (valueA, valueB, fieldType) => valueA.x == valueB.x
              }
            case Lt(a, b) =>
              validateFilter(a, b, "not less than", globalCatalog) {
                (valueA, valueB, fieldType) =>
                  fieldType match {
                    case StringT =>
                      filterLt(implicitly[Ordering[String]])(valueA.x)(
                        valueB.x
                      )
                    case IntegerT =>
                      filterLt(implicitly[Ordering[Int]])(valueA.x)(
                        valueB.x
                      )
                    case _ => false
                  }
              }
            case Gt(a, b) =>
              validateFilter(a, b, "not greater than", globalCatalog) {
                (valueA, valueB, fieldType) =>
                  fieldType match {
                    case StringT =>
                      filterGt(implicitly[Ordering[String]])(valueA.x)(
                        valueB.x
                      )
                    case IntegerT =>
                      filterGt(implicitly[Ordering[Int]])(valueA.x)(
                        valueB.x
                      )
                    case _ => false
                  }
              }
            case _ => eitherUnit
          }
        }
      case Group(key, agg, parent) =>
        validateSchema(parent, globalCatalog, aliasMap).flatMap { _ =>
          globalCatalog.tableName(key.tableName) match {
            case None =>
              s"Table ${key.tableName} in Aggregated field ${key.formatAsSql} could not be found "
                .asLeft[Unit]
            case Some(tableName)
                if (globalCatalog.schema
                  .get(tableName)
                  .exists(
                    _.fields
                      .find(_.name == key.name)
                      .isEmpty
                  )) =>
              s"Field ${key.name} from Table ${key.tableName} in Aggregated field ${key.formatAsSql} could not be found "
                .asLeft[Unit]

            // Check field is in projections
            case _ =>
              parent match {
                case Filter(pred, parent) =>
                  parent match {
                    case Projection(outSchema, inSchema, _)
                        if inSchema.exists(f =>
                          f.tableName == key.tableName && f.name == key.name
                        ) =>
                      eitherUnit
                    case _ =>
                      s"Aggregated field ${key.formatAsSql} could not be found in projections"
                        .asLeft[Unit]
                  }
                case Projection(outSchema, inSchema, _)
                    if inSchema.exists(f =>
                      f.tableName == key.tableName && f.name == key.name
                    ) =>
                  eitherUnit
                case _ =>
                  s"Aggregated field ${key.formatAsSql} could not be found in projections"
                    .asLeft[Unit]
              }
          }
        }
      case s @ _ => Left(s"Invalid schema given: $s not expected")
    }
  }

  def validateFilter(
      left: Ref,
      right: Ref,
      predicateErrorMessage: String,
      globalCatalog: DatabaseCatalog[String]
  )(
      predicate: (Value, Value, FieldType) => Boolean
  ): Either[String, Unit] = {
    (left, right) match {
      case (Field(name, tableName), Value(b, _))
          if globalCatalog.tableName(tableName).isEmpty =>
        s"Field $name in table $tableName does not exists".asLeft[Unit]
      case (Value(a, _), Field(name, tableName))
          if globalCatalog.tableName(tableName).isEmpty =>
        s"Field $name in table $tableName does not exists".asLeft[Unit]
      case (Value(a, aType), Value(b, bType)) if aType != bType =>
        s"Value a $a and b $b are not of the same type".asLeft[Unit]
      case (va @ Value(a, aType), vb @ Value(b, bType)) =>
        (aType match {
          case StringT  => eitherUnit
          case IntegerT => eitherUnit
          case _        => s"Unhandled value type $aType".asLeft[Unit]
        }).flatMap(_ =>
          if (predicate(va, vb, aType))
            s"Value a $a is not $predicateErrorMessage Value $b".asLeft[Unit]
          else eitherUnit
        )

      case _ => eitherUnit
    }
  }

  private def filterLt[T]: Ordering[T] => Any => Any => Boolean =
    order => a => b => order.lt(a.asInstanceOf[T], b.asInstanceOf[T])

  private def filterGt[T]: Ordering[T] => Any => Any => Boolean =
    order => a => b => order.gt(a.asInstanceOf[T], b.asInstanceOf[T])

  private def isFieldInSchema(
      globalCatalog: DatabaseCatalog[String],
      aliasMap: Map[String, String],
      field: Field
  ): Boolean = {
    globalCatalog
      .getSchema(
        field.tableName,
        aliasMap.getOrElse(field.tableName, field.tableName)
      )
      .get
      .fields
      .find(_.name == field.name)
      .isDefined
  }

  private def eitherUnit: Either[String, Unit] = Right(())
  private def joinValidatePartial
      : DatabaseCatalog[String] => Map[String, String] => PartialFunction[
        Join,
        Either[String, Unit]
      ] =
    globalCatalog =>
      aliasMap => {
        case j => validateSchema(j, globalCatalog, aliasMap)
      }
}

object QueryValidator extends QueryValidator
