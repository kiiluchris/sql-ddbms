package db
package sql
import schema._
import QueryAST._

trait QueryProcessor[T] {
  def explicitlyNamesOuterInJoins: Boolean
  def execQuery(globalCatalog: DatabaseCatalog[String]): Operator => T
}
