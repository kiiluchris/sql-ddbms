package db
package sql
import schema._
import QueryAST._

trait QueryReverser extends QueryProcessor[String] {
  def aliasFormatter(tableName: String, alias: String): String

  def execOp(globalCatalog: DatabaseCatalog[String], op: Operator)(
      convert: => String
  ): String =
    op match {
      case Projection(columnAliases, columns, parent) =>
        execOp(globalCatalog, parent) {
          List(
            "SELECT",
            columns
              .zip(columnAliases)
              .map { p =>
                TableName(p._1, p._2.filter(_ != p._1.formatAsSql))
                  .formatAsSql(aliasFormatter _)
              }
              .mkString(", "),
            "FROM"
          ).mkString(" ")
        }
      case OverallJoin(parent, cross, joins) =>
        List(
          convert,
          parent.formatAsSql(aliasFormatter _),
          cross.map(_.formatAsSql(aliasFormatter _)).mkString(", "),
          joins.map(formatJoin _).mkString(" ")
        ).mkString(" ")

      case _ => ""
    }
  def formatJoin(join: Join): String =
    List(
      join.joinTypeAsSql(this.explicitlyNamesOuterInJoins),
      join.tableName
        .formatAsSql(aliasFormatter _),
      "ON",
      join.predicate.formatAsSql
    ).mkString(" ")

  def optimizeOp(schema: DatabaseCatalog[String]): Operator => Operator = {
    // case Projection(outSchema, inSchema, parent) =>
    // outSchema.zip(inSchema).filter(((actual, alias)) => schema.values)
    //     Projection()
    case _ => NoJoin
  }

  def optimizeQuery(schema: DatabaseCatalog[String]): Operator => Operator =
    optimizeOp(schema)

  def execQuery(schema: DatabaseCatalog[String]): Operator => String =
    execOp(schema, _)("")
}
