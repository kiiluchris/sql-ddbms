import Dependencies._

ThisBuild / scalaVersion := "2.13.1"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .settings(
    name := "DB Controller",
    resolvers ++= Seq(
      Resolver.sonatypeRepo("releases"),
      Resolver.sonatypeRepo("snapshots")
    ),
    libraryDependencies ++= Seq(
      guice,
      scalaTest % Test,
      "com.typesafe" % "config" % "1.4.0",
      "dev.zio" %% "zio" % "1.0.0-RC18-1",
      "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2",
      "co.fs2" %% "fs2-core" % "2.2.1",
      "co.fs2" %% "fs2-io" % "2.2.1",
      "com.chuusai" %% "shapeless" % "2.3.3",
      "com.typesafe.slick" %% "slick" % "3.3.2",
      "org.slf4j" % "slf4j-nop" % "1.7.26",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.3.2",
      "org.postgresql" % "postgresql" % "42.2.5", //org.postgresql.ds.PGSimpleDataSource dependency
      "mysql" % "mysql-connector-java" % "6.0.6",
      "com.microsoft.sqlserver" % "mssql-jdbc" % "8.2.1.jre8",
      "org.scala-lang" % "scala-reflect" % scalaVersion.value,
      "com.typesafe.akka" %% "akka-http" % "10.1.11",
      "com.typesafe.akka" %% "akka-stream" % "2.5.26",
      "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.11"
    ),
    scalacOptions ++= Seq(
      "-deprecation"
    )
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
